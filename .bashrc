#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Check if tmux is installed, then run it
# if command -v tmux>/dev/null; then
#   [[ ! $TERM =~ screen ]] && [ -z $TMUX ] && exec tmux
# fi

if [ "$EUID" -ne 0 ]
	then export PS1="\[$(tput bold)\]\[$(tput setaf 1)\][\[$(tput setaf 3)\]\u\[$(tput setaf 2)\]@\[$(tput setaf 4)\]\h \[$(tput setaf 5)\]\W\[$(tput setaf 1)\]]\[$(tput setaf 7)\]\\$ \[$(tput sgr0)\]"
	else export PS1="\[$(tput bold)\]\[$(tput setaf 1)\][\[$(tput setaf 3)\]ROOT\[$(tput setaf 2)\]@\[$(tput setaf 4)\]$(hostname | awk '{print toupper($0)}') \[$(tput setaf 5)\]\W\[$(tput setaf 1)\]]\[$(tput setaf 7)\]\\$ \[$(tput sgr0)\]"
fi

# source ~/.scripts/git-prompt.sh
# export PS1='[\u@\h \W$(__git_ps1 " (%s)")]\$ '

# (cat ~/.cache/wal/sequences &)

alias ls='ls -lah --group-directories-first --color=auto'
alias all="(pacman -Qet && pacman -Qm) | sort -u | less"
alias n="clear && neofetch"
alias r="ranger"
alias rr="source ~/.bashrc"
alias ..="cd .."
alias blk="lsblk -o name,type,fstype,label,partlabel,model,mountpoint,size"
alias browser="waterfox"
alias xrdbm="xrdb -merge ~/.Xresources"
alias sc="scrot %Y-%m-%d_$wx$h.png' -e 'mv $f ~/Images/"
alias grep="grep --color=always"
alias mix="ncpamixer"
alias pulsecheck="sudo fuser -v /dev/snd/*"

export PATH=$PATH:$HOME/.scripts:$HOME/Programs:$HOME/.local/bin
export EDITOR="vim"
export TERMINAL="urxvt"
export BROWSER="waterfox"
export XDG_CURRENT_DESKTOP="i3"
export GOPATH="/mnt/hisui/Programming/go"

# 0-15: System colors
#     0  black         8  dark gray
#     1  dark red      9  red
#     2  dark green    10 green
#     3  dark yellow   11 yellow
#     4  dark blue     12 blue
#     5  dark purple   13 purple
#     6  dark cyan     14 cyan
#     7  light gray    15 white

colors(){
    for x in {0..8}; do
        for i in {30..37}; do
            for a in {40..47}; do
                echo -ne "\e[$x;$i;$a""m\\\e[$x;$i;$a""m\e[0;37;40m "
            done
            echo
        done
    done
    echo ""
}

colors2(){
  (x=`tput op` y=`printf %76s`;for i in {0..256};do o=00$i;echo -e ${o:${#o}-3:3} `tput setaf $i;tput setab $i`${y// /=}$x;done)
}

pacbackup(){
  pacman -Qqe > ~/.pkglist.txt
}

function up()
{
    for i in `seq 1 $1`;
    do
        cd ../
    done;
}

pulserestart(){
  pulseaudio -k;
  pulseaudio --start;
}

extract() {
    local c e i

    (($#)) || return

    for i; do
        c=''
        e=1

        if [[ ! -r $i ]]; then
            echo "$0: file is unreadable: \`$i'" >&2
            continue
        fi

        case $i in
            *.t@(gz|lz|xz|b@(2|z?(2))|a@(z|r?(.@(Z|bz?(2)|gz|lzma|xz)))))
                   c=(bsdtar xvf);;
            *.7z)  c=(7z x);;
            *.Z)   c=(uncompress);;
            *.bz2) c=(bunzip2);;
            *.exe) c=(cabextract);;
            *.gz)  c=(gunzip);;
            *.rar) c=(unrar x);;
            *.xz)  c=(unxz);;
            *.zip) c=(unzip);;
            *)     echo "$0: unrecognized file extension: \`$i'" >&2
                   continue;;
        esac

        command "${c[@]}" "$i"
        ((e = e || $?))
    done
    return "$e"
}

PATH="/home/saber/perl5/bin${PATH:+:${PATH}}"; export PATH;
PERL5LIB="/home/saber/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}"; export PERL5LIB;
PERL_LOCAL_LIB_ROOT="/home/saber/perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"; export PERL_LOCAL_LIB_ROOT;
PERL_MB_OPT="--install_base \"/home/saber/perl5\""; export PERL_MB_OPT;
PERL_MM_OPT="INSTALL_BASE=/home/saber/perl5"; export PERL_MM_OPT;
