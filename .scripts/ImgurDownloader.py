#!/bin/python3

'''Download albums from imgur'''
import os
import argparse
import json
import urllib.request
import requests


def createDirectory(path):
    '''Create directory for album'''

    if len(path) > 1:
        try:
            os.mkdir(path)
        except FileExistsError as exception:
            print("Directory ./{}/ already exists".format(path))
        finally:
            os.chdir(path)
    else:
        print("Usage: imgurscraper <album id>")
        exit(0)


def getJSON(imgur_id):
    '''Get the JSON object from address'''

    url = ("https://imgur.com/ajaxalbums/getimages/{}/hit.json?all=true".format(imgur_id))
    response = requests.get(url)
    return json.loads(response.text)


def saveImgur(data):
    '''Save album images into folder, return to parent directory'''

    json_data = data
    print("Saving {} images".format(str(json_data["data"]["count"])))

    for image in json_data["data"]["images"]:
        if image["ext"] == ".gif":
            idd = image['hash'] + ".mp4"
            idd = "{}.mp4".format(image["hash"])
        else:
            idd = image["hash"] + image["ext"]
            full_url = "https://i.imgur.com/{}".format(idd)

        if os.path.isfile(idd):
            print("File {} already exists.".format(idd))
        else:
            urllib.request.urlretrieve(full_url, idd)
            print(idd)
    os.chdir("..")


def sanitize(album):
    '''Format album id correctly'''

    if album[-1] == "/":
        album = album[:-1]
    else:
        pass
    return album.split("/")[-1].strip()


def download(album):
    '''Initiate download'''

    createDirectory(sanitize(album))
    saveImgur(getJSON(sanitize(album)))


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Process imgur albums", prog="ImgurScrape")
    parser.add_argument("-c", "--command", help="Input from command line", nargs="*")
    parser.add_argument("-f", "--file", help="Input from file")
    parser.add_argument("-l", "--live", help="Input from file", action="store_true")

    args = parser.parse_args()

    if args.file:
        with open(args.file) as albums:
            for album in albums:
                download(album)
    if args.command:
        for album in args.command:
            download(album)
    if args.live:
        while True:
            entry = input("Waiting for link of album to download:\n")
            if entry == "q":
                exit(0)
            else:
                download(entry)
    exit(0)
