  hi Normal          ctermbg=NONE  ctermfg=250     guibg=NONE     guifg=#A57A9E  cterm=NONE      gui=NONE
  hi Conceal         ctermbg=NONE ctermfg=124    guibg=NONE     guifg=#B3B8C4  cterm=NONE      gui=NONE
  hi NonText         ctermbg=NONE ctermfg=8      guibg=NONE     guifg=#3E4853  cterm=NONE      gui=NONE
  hi Title           ctermbg=NONE ctermfg=231    guibg=NONE     guifg=#FFFFFF  cterm=NONE      gui=NONE
  hi Constant        ctermbg=NONE ctermfg=186    guibg=NONE     guifg=#C5735E  cterm=NONE      gui=NONE
  hi Function        ctermbg=NONE ctermfg=105      guibg=NONE     guifg=#CC6666  cterm=NONE      gui=NONE
  hi Type            ctermbg=NONE ctermfg=5      guibg=NONE     guifg=#CC6666  cterm=NONE      gui=NONE
  hi Identifier      ctermbg=NONE ctermfg=84    guibg=NONE     guifg=#E5C078  cterm=NONE      gui=NONE
  hi PreProc         ctermbg=NONE ctermfg=92    guibg=NONE     guifg=#85A7A5  cterm=NONE      gui=NONE
  hi Special         ctermbg=NONE ctermfg=183    guibg=NONE     guifg=#7D8FA3  cterm=NONE      gui=NONE
  hi SpecialKey      ctermbg=NONE ctermfg=63     guibg=NONE     guifg=#4C5966  cterm=NONE      gui=NONE
  hi Statement       ctermbg=NONE ctermfg=78     guibg=NONE     guifg=#A57A9E  cterm=NONE      gui=NONE
  hi Noise           ctermbg=NONE ctermfg=210     guibg=NONE     guifg=#A57A9E  cterm=NONE      gui=NONE
  hi Comment       ctermbg=NONE ctermfg=59     guibg=NONE     guifg=#515F6A  cterm=italic    gui=italic

  hi Cursor          ctermbg=NONE ctermfg=NONE   guibg=#6C6C6C  guifg=NONE     cterm=NONE      gui=NONE
  hi CursorColumn    ctermbg=0    ctermfg=NONE   guibg=#303030  guifg=NONE     cterm=NONE      gui=NONE
  hi CursorLine      ctermbg=235  ctermfg=NONE   guibg=#303030  guifg=NONE     cterm=NONE      gui=NONE


  hi FoldColumn      ctermbg=NONE ctermfg=242    guibg=#1C1C1C  guifg=#6C6C6C  cterm=NONE      gui=NONE
  hi Folded          ctermbg=NONE ctermfg=242    guibg=#1C1C1C  guifg=#6C6C6C  cterm=NONE      gui=NONE
  hi VertSplit       ctermbg=232  ctermfg=233    guibg=#444444  guifg=#444444  cterm=NONE      gui=NONE


  hi Visual          ctermbg=238 ctermfg=NONE   guibg=#404040  guifg=NONE     cterm=NONE      gui=NONE
  hi VisualNOS       ctermbg=8    ctermfg=NONE   guibg=NONE     guifg=NONE     cterm=bold      gui=bold


  hi DiffAdd         ctermbg=65   ctermfg=232    guibg=#5F875F  guifg=#080808  cterm=NONE      gui=NONE
  hi DiffChange      ctermbg=237  ctermfg=NONE   guibg=#3A3A3A  guifg=NONE     cterm=NONE      gui=NONE
  hi DiffDelete      ctermbg=234  ctermfg=9      guibg=NONE     guifg=#CC6666  cterm=NONE      gui=NONE
  hi DiffText        ctermbg=60   ctermfg=251    guibg=#5F5F87  guifg=#D0D0D0  cterm=NONE      gui=NONE

  " Vim-Gittgutter
  hi GitGutterAdd ctermbg=NONE   ctermfg=76      guibg=#5F0000  guifg=#CC6666  cterm=NONE      gui=NONE
  hi GitGutterChange ctermbg=NONE   ctermfg=33      guibg=#5F0000  guifg=#CC6666  cterm=NONE      gui=NONE
  hi GitGutterDelete ctermbg=NONE   ctermfg=160      guibg=#5F0000  guifg=#CC6666  cterm=NONE      gui=NONE
  hi GitGutterChangeDelete ctermbg=NONE   ctermfg=178      guibg=#5F0000  guifg=#CC6666  cterm=NONE      gui=NONE

  hi LineNr          ctermbg=NONE ctermfg=59     guibg=#111314  guifg=#3E4853  cterm=NONE      gui=NONE
  hi CursorLineNr    ctermbg=234  ctermfg=2      guibg=NONE     guifg=#808080  cterm=NONE      gui=NONE

  hi Todo            ctermbg=88   ctermfg=NONE   guibg=NONE     guifg=NONE cterm=NONE   gui=reverse
  hi Error           ctermbg=52   ctermfg=12     guibg=NONE     guifg=#AF5F5F cterm=NONE gui=reverse
  hi ErrorMsg        ctermbg=88   ctermfg=9      guibg=NONE     guifg=#C5735E  cterm=NONE      gui=NONE
  " hi Question        ctermbg=88 ctermfg=214    guibg=NONE     guifg=#FFAF00  cterm=NONE      gui=NONE
  " hi ModeMsg         ctermbg=88 ctermfg=249    guibg=NONE     guifg=#808080  cterm=NONE      gui=NONE
  " hi MoreMsg         ctermbg=88 ctermfg=249    guibg=NONE     guifg=#808080  cterm=NONE      gui=NONE
  " hi WarningMsg      ctermbg=88 ctermfg=12     guibg=NONE     guifg=#7D8FA3  cterm=NONE      gui=NONE

  hi SpellBad        ctermbg=52   ctermfg=9      guibg=#5F0000  guifg=#CC6666  cterm=NONE      gui=NONE
  hi SpellRare       ctermbg=53   ctermfg=13     guibg=#5F005F  guifg=#B294BB  cterm=NONE      gui=NONE
  hi SpellCap        ctermbg=94   ctermfg=255    guibg=#00005F  guifg=#81A2BE  cterm=NONE      gui=NONE
  hi SpellLocal      ctermbg=24   ctermfg=14     guibg=#005F5F  guifg=#8ABEB7  cterm=NONE      gui=NONE

  " Shell
  hi link shStatement Type
  hi link shSet Type

  " Javascript
  hi jsFuncCall       ctermbg=NONE ctermfg=197       cterm=NONE      gui=NONE
  hi jsObject         ctermbg=NONE ctermfg=99        cterm=NONE      gui=NONE
  hi jsObjectKey      ctermbg=NONE ctermfg=99        cterm=NONE      gui=NONE
  hi jsObjectProp     ctermbg=NONE ctermfg=111       cterm=NONE      gui=NONE
  hi jsObjectValue    ctermbg=NONE ctermfg=111       cterm=NONE      gui=NONE
  hi jsVariableDef    ctermbg=NONE ctermfg=255       cterm=NONE      gui=NONE
  hi jsArrowFunction  ctermbg=NONE ctermfg=197       cterm=NONE      gui=NONE
  " hi jsFuncArgs       ctermbg=NONE ctermfg=5       cterm=NONE      gui=NONE
  hi jsFuncArgs       ctermbg=NONE ctermfg=255       cterm=NONE      gui=NONE
  " hi jsParen          ctermbg=NONE ctermfg=5       cterm=NONE      gui=NONE
  hi jsParen          ctermbg=NONE ctermfg=255       cterm=NONE      gui=NONE
  hi jsBrackets       ctermbg=NONE ctermfg=197       cterm=NONE      gui=NONE
  hi jsModuleKeyword  ctermbg=NONE ctermfg=78        cterm=NONE      gui=NONE
  hi link jsDestructuringBlock jsModuleKeyword
  hi link jsFuncBlock Special
  hi link jsBlock jsParen


  " JSX
  hi link xmlEndTag Function
  hi xmlAttrib ctermbg=NONE ctermfg=5 cterm=italic gui=NONE

  " HTML
  hi link htmlArg xmlAttrib
  hi link htmlTag Identifier
  hi link htmlEndTag Identifier
  hi link htmlTagName Identifier
  hi link htmlScriptTag Identifier
  hi link htmlSpecialTagName Identifier


  " Vim Groups
  hi link vimVar Type
  hi link vimIsCommand Type
  " hi vimMapLhs        ctermbg=NONE ctermfg=02      guibg=NONE     guifg=#A57A9E  cterm=NONE      gui=NONE
  " hi vimMap           ctermbg=NONE ctermfg=02      guibg=NONE     guifg=#A57A9E  cterm=NONE      gui=NONE
  " hi vimHiCtermFgBg   ctermbg=NONE ctermfg=02      guibg=NONE     guifg=#A57A9E  cterm=NONE      gui=NONE
  " hi vimHiCterm       ctermbg=NONE ctermfg=09      guibg=NONE     guifg=#A57A9E  cterm=NONE      gui=NONE
  " hi vimHiGui         ctermbg=NONE ctermfg=09      guibg=NONE     guifg=#A57A9E  cterm=NONE      gui=NONE
  " hi vimHiGuiFgBg     ctermbg=NONE ctermfg=03      guibg=NONE     guifg=#A57A9E  cterm=NONE      gui=NONE

  " hi TabLine      ctermfg=165  ctermbg=165     cterm=NONE
  " hi TabLineFill  ctermfg=165  ctermbg=165     cterm=NONE
  " hi TabLineSel   ctermfg=165  ctermbg=165  cterm=NONE


  " Indent Plugin
  hi IndentGuidesOdd  ctermbg=232
  hi IndentGuidesEven ctermbg=233
